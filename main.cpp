#include <stdio.h>
#include <iostream>
using namespace std;

void fibbo();
void factorial();
void arm();
void pc();
void table();
void reverse();

int main()
{
    int num;
    printf("\n");
    printf("What you want to do !\n");
    printf("1.Find fibbonacci series\n");
    printf("2.Find factorial\n");
    printf("3.Find armstrong number\n");
    printf("4.Find prime or composit number\n");
    printf("5.Find table of any number\n");
    printf("6.Find reverse of any number\n");

    printf("Enter your number : ");
    scanf("%d", &num);
    switch (num)
    {
    case 1:
        fibbo();
        break;
    case 2:
        factorial();
    case 3:
        arm();
    case 4:
        pc();
    case 5:
        table();
    case 6:
        reverse();
    default:
        break;
    }
    return 0;
}
void fibbo()
{
    int x, y, z;
    printf("Fibbonacci program!\n");
    printf("Enter your first number : ");
    scanf("%d", &x);
    printf("Enter your second number : ");
    scanf("%d", &y);
    // scanf("%d",&z);
    printf("%d  ", x);
    printf("%d  ", y);

    for (int i = 1; i <= 10; i++)
    {
        z = x + y;
        printf("%d  ", z);
        // x y z
        x = y;
        y = z;
    }

    // return 0;
    main();
}

void factorial()
{
    int num_factorian, sum_factorial = 1;
    printf("Factorial Program !\n");
    printf("Enter any number : ");
    scanf("%d", &num_factorian);
    for (int i = 1; i <= num_factorian; i++)
    {
        // 1 * 2 * 3 * 4 * 5
        sum_factorial = sum_factorial * i;
    }
    printf("The factorial of %d is : %d", num_factorian, sum_factorial);
    main();
    // return 0;
}

void arm()
{
    int num_armstrong;
    int sum_armstrong = 0;
    printf("Armstrong programe!\n");
    printf("Enter any number : ");
    scanf("%d", &num_armstrong);
    int org_num_armstrong = num_armstrong;
    while (num_armstrong != 0)
    {
        int mod_armstrong = num_armstrong % 10;
        // printf("%d\n", mod);
        sum_armstrong = (mod_armstrong * mod_armstrong * mod_armstrong) + sum_armstrong;
        // printf("%d\n", sum);
        num_armstrong = num_armstrong / 10;
        // printf("%d\n", num);
    }
    if (sum_armstrong == org_num_armstrong)
    {
        printf("%d is a armstrong number\n", org_num_armstrong);
    }
    else
    {
        printf("%d is not a armstrong number\n", org_num_armstrong);
    }
    main();
    // return 0;
}

void pc()
{
    int num1_pc;
    int mod_pc;
    int sum_pc;
    printf("Prime composit program!\n");
    printf("Enter any number : ");
    scanf("%d", &num1_pc);
    for (int i = 1; i <= num1_pc; i++)
    {
        mod_pc = num1_pc % i;
        if (mod_pc == 0)
        {
            sum_pc += 1;
        }
    }
    if (sum_pc==2)
    {
        printf("%d is a prime number \n",num1_pc);
        
    }else{
        printf("%d is a composit number \n",num1_pc);
    }
    
    main();
    // return 0;
}


void table(){
    int num_table;
    printf("Table program !\n");
    printf("Enter your number : ");
    scanf("%d", &num_table);
    for (int i = 1; i <= 10; i++)
    {
        printf("%d x %d = %d\n", num_table, i, num_table * i);
    }
    main();
    // return 0;
}


void reverse(){
    int num_reverse,mod_reverse;
    int sum_reverse = 0;

    printf("Reverse program!\n");
    printf("Enter any number : ");
    scanf("%d", &num_reverse);
    while (num_reverse > 0)
    {
        mod_reverse = num_reverse % 10;
        sum_reverse = (sum_reverse * 10) + mod_reverse;
        num_reverse = num_reverse / 10;
    }
    // printf("%d", 125 % 10);
    printf("The reverse of the given number : %d",sum_reverse);
    main();
    // return 0;
}